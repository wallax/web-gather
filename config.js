module.exports = {
	'accept': 'text/html,application/xhtml+xml,*/*;q=0.8',
	'accept-encoding': 'gzip',
	'cache-control': 'max-age=0',
	'user-agent': 'Mozilla/5.0 (X11) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.101 Gather/1.4.2'
}