'use strict';

var requost = require('./requost.js');
var assert = require('assert');

// 默认请求头
var request_headers = require('./config.js');

function gather(options, cb){
	assert('object' == typeof options);
	assert('function' == typeof cb);
	assert('string' == typeof options.url);
	assert('number' == typeof options.pages || 'string' == typeof options.pages || options.pages instanceof Array);
	assert(options.filter instanceof RegExp);

	// 请求地址格式，eg. http://example.com/list/page/{page}
	var url = options.url;

	// 将单个页数转换为数组
	if('string' == options.pages || 'number' == typeof options.pages){
		options.pages = [options.pages];
	}
	var pages = options.pages;

	// 存放结果的数组
	var result = [];

	// 结果计数
	var count = 0, success = 0, fail = 0;

	// 默认请求配置
	var config = {
		method: 'GET',
		headers: request_headers,
		dataType: 'string'
	};

	// 设置自定义配置
	if('object' == typeof options.request){
		var req = options.request;
		if('string' == typeof req.method) config.method = req.method.toUpperCase();
		if(req.data) config.data = req.data;
		if('string' == typeof req.dataType) config.dataType = req.dataType;
		if('string' == typeof req.charset) config.charset = req.charset;
		if('string' == typeof req.encoding) config.encoding = req.encoding;
		if('object ' == typeof req.headers){
			for(var i in req.headers) config.headers[i] = req.headers[i];
		}
	}

	// 循环请求任务
	for(var i = 0; i < pages.length; i++){
		requost(url.replace('{page}', pages[i]), config, function(res){
			count++;
			if(!res){
				fail++;
				return;
			}

			if(res instanceof Buffer) res = res.toString();

			// 执行预处理格式化
			if(options.before instanceof Array){
				for(var i = 0; i < options.before.length; i++){
					if((!options.before[i].rule instanceof RegExp) || 'string' != typeof options.before[i].replace) continue;

					res = res.replace(options.before[i].rule, options.before[i].replace);
				}
			}
			// 截取信息，filter必须带子查询
			var arr = res.match(options.filter);

			// 单项目模式
			if(options.single) arr = arr.slice(1);

			// 执行后处理格式化
			if(options.after instanceof Array){
				for(var j = 0; j < arr.length; j++){
					for(var i = 0; i < options.after.length; i++){
						if((!options.after[i].rule instanceof RegExp) || 'string' != typeof options.after[i].replace) continue;

						arr[j] = arr[j].replace(options.after[i].rule, options.after[i].replace);
					}
				}
			}

			// 添加结果
			success++;
			if(options.single){
				result.push(arr);
			}else{
				result = result.concat(result, arr);
			}
			
			// 判断执行次数，如果处理完成就执行回调
			if(count == pages.length){
				cb.call(null, {
					count: count,
					success: success,
					fail: fail
				}, result);
			}
		});
	}
}

module.exports = gather;