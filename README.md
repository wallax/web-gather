# Web-gather

信息采集模板

版本： beta

## 使用方法

先下载本项目，然后`npm i`安装依赖

```javascript

var gather = require('web-gather');

// 采集规则
var options = {
	// 采集地址，{page}用于替换页数，必须
	url: 'http://example.com/page/{page}',

	// 采集页数列表，也可以是文章id之类的东西，可以是字符串数组，必须
	pages: [1, 2, 3],

	// 是否为单项模式，比如文章正文采集，多项模式为单页多个内容
	// 单项模式不使用g修饰符，多项则使用g修饰符
	// 默认多项
	single: 0,

	// 请求选项
	request: {
		// http请求方法，默认GET
		method: 'GET',

		// 字符集，默认utf8
		charset: 'gb2312',

		// 类型，默认utf8，一般不设置此项
		encoding: 'utf8'

		// 要POST的数据，可以是对象或者字符串、Buffer
		data: {
			ts: 123
		},

		// http请求头
		headers: {
			'accept': 'text/html'
		}
	},

	// 预处理替换，用于采集之前整理格式
	before: [{
		// 替换规则，正则表达式
		rule: />\s*</gm,

		// 替换的内容
		replace: '><'
	}],

	// 采集格式，正则表达式，必须
	filter: /<h1>.*?<\/h1>/g

	// 采集后整理格式，同预处理，在采集完成后对每个结果执行
	after: [{
		rule: /<br.*?>/g,
		replace: "\n"
	}]
};

gather(options, function(info, res){
	// 采集统计，info.count=总页数，info.success=成功数，info.fail=失败数
	console.log(info);

	// 采集内容，单项模式为二维数组，多项模式为一维数组
	console.log(res);
});

```

## License

**MIT**