'use strict';

var http = require('http');
var assert = require('assert');
var qs = require('querystring');
var zlib = require('zlib');
var iconv = require('iconv-lite');

var requost = function(){
	assert(arguments.length >= 2, 'Require up 2 arguments');

	// 请求地址
	var url = arguments[0];
	assert('string' == typeof url, 'Invalid url');

	// 选项
	var options = arguments.length > 2 ? arguments[1] : {};
	assert('object' == typeof options, 'Invalid options');

	// 回调函数
	var cb = arguments.length == 3 ? arguments[2] : arguments[1];
	assert('function' == typeof cb, 'Invalid callback');

	// 请求方法
	var method = 'string' == typeof options.method ? options.method : 'GET';
	// post数据
	var post_data = '';

	var req_options = require('url').parse(url);

	req_options.method = method;
	// 添加自定义header
	req_options.headers = 'object' == typeof options.headers ? options.headers : {};

	if(method == 'POST' || method == 'PUT'){
		// 设置post_data
		if('string' == typeof options.data){
			post_data = escape(options.data);
		}else if('object' == typeof options.data){
			post_data = escape(qs.stringify(options.data));
			req_options.headers['content-type'] = 'application/x-www-form-urlencoded';
		}
	}

	var req = http.request(req_options, function(res){
		var buf;
		// 设置编码
		if(options.encoding) res.setEncoding(options.encoding);

		function unencoding(err, buf){
			assert(err == null);
			var result;
			if(options.dataType == 'json'){
				try{
					result = JSON.parse(buf.toString());
				}catch(e){
					// 解析json出错，返回原始数据
					cb.call(null, buf.toString());
					return;
				}

				cb.call(null, result);
			}else if(options.dataType == 'string'){
				if('string' == typeof options.charset){
					result = iconv.decode(buf, options.charset);
				}else{
					result = buf.toString();
				}
				cb.call(null, result);
			}else{
				cb.call(null, buf);
			}
		};

		res.on('data', function(chunk){
			buf = Buffer.concat([(buf ? buf : new Buffer('')), new Buffer(chunk)]);
		}).on('end', function(){
			if(res.headers['content-encoding']){
				if(res.headers['content-encoding'] == 'gzip'){
					zlib.gunzip(buf, unencoding);
				}else if(res.headers['content-encoding'] == 'deflate'){
					zlib.inflate(buf, unencoding);
				}else{
					throw new Error('Unsupported encoding')
				}
			}else{
				unencoding(null, buf);
			}
		}).on('error', function(){
			cb.call(null);
		});
	});
	req.on('error', function(){
		cb.call(null);
	});
	if(post_data) req.write(post_data);
	req.end();
}



module.exports = requost;